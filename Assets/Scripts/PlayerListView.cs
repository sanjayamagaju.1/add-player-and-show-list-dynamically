using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerListView : MonoBehaviour
{

  // public string name, level, health;

  public GameObject playerPrefab;
  public Transform listHolder;

  public void Start() {
    foreach (Players players in inputSceneScript.players)
    {
      GameObject player = Instantiate(playerPrefab, listHolder);

      player.transform.GetChild(0).GetComponent<TMP_Text>().text = "Name:" + " " + players.playerName;
      player.transform.GetChild(1).GetComponent<TMP_Text>().text = "Level:" + " " + players.playerLevel;
      player.transform.GetChild(2).GetComponent<TMP_Text>().text = "Health:" + " " + players.playerHealth;
          
    }

    // Debug.Log(inputSceneScript.players.playerName);
  }
}
