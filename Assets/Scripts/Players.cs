using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Players 
{
    public string playerName;
    public string playerLevel;
    public string playerHealth;

    public Players(string newName, string newLevel, string newHealth) 
    {
        playerName = newName;
        playerLevel = newLevel;
        playerHealth = newHealth;
    }
}
