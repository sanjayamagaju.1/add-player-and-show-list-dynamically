using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class inputSceneScript : MonoBehaviour
{
    public static List<Players> players = new();
    public TMP_InputField PlayerName, PlayerLevel, PlayerHealth;
    public TextMeshProUGUI popUp;
    

    public void addPlayer(){
        if (PlayerName.text == "" || PlayerLevel.text == "" || PlayerHealth.text == "")
        {
            popUp.text = "Some input fields are invalid!!!";
        }
        else{
            popUp.text = "Player successfully added :)";
            players.Add(new Players(PlayerName.text, PlayerLevel.text, PlayerHealth.text));

            PlayerName.text = "";
            PlayerLevel.text = "";
            PlayerHealth.text = "";
        }
        
    }
}
